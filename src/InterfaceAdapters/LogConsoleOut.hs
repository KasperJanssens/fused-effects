{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}

module InterfaceAdapters.LogConsoleOut where

import Control.Algebra ((:+:) (..), Algebra, alg)
import Control.Carrier.Reader (ReaderC, ask, runReader)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Database.Persist.Sqlite (SqliteConnectionInfo)
import UseCases.Log

newtype LogConsoleOutC m a = LogConsoleOutC {runTraceConsoleOut :: m a}
  deriving (Applicative, Functor, Monad, MonadIO)

instance (MonadIO m, Algebra sig m) => Algebra (Log :+: sig) (LogConsoleOutC m) where
  alg hdl sig ctx = case sig of
    L (Error m) -> ctx <$ liftIO (putStrLn $ "Error : " ++ m)
    L (Info m) -> ctx <$ liftIO (putStrLn $ "Info : " ++ m)
    L (Warn m) -> ctx <$ liftIO (putStrLn $ "Warn : " ++ m)
    L (Debug m) -> ctx <$ liftIO (putStrLn $ "Debug : " ++ m)
    R other -> LogConsoleOutC (alg (runTraceConsoleOut . hdl) other ctx)
