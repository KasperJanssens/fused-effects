{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}

module InterfaceAdapters.LogInMem where

import Control.Algebra ((:+:) (..), Algebra, alg)
import Control.Carrier.Writer.Strict (WriterC, runWriter, tell)
import Control.Monad.IO.Class (MonadIO, liftIO)
import UseCases.Log

runLog :: Functor m => LogInMemC m a -> m ([String], a)
runLog (LogInMemC m) = let res = runWriter m in fmap (\(f, a) -> (f [], a)) res

newtype LogInMemC m a = LogInMemC {runLogInMemC :: WriterC ([String] -> [String]) m a}
  deriving (Applicative, Functor, Monad, MonadIO)

instance (MonadIO m, Algebra sig m) => Algebra (Log :+: sig) (LogInMemC m) where
  alg hdl sig ctx = case sig of
    L (Error m) -> ctx <$ LogInMemC (tell (("Error " ++ m) :))
    L (Info m) -> ctx <$ LogInMemC (tell (("Info " ++ m) :))
    L (Warn m) -> ctx <$ LogInMemC (tell (("Warn " ++ m) :))
    L (Debug m) -> ctx <$ LogInMemC (tell (("Debug " ++ m) :))
    R other -> LogInMemC (alg (runLogInMemC . hdl) (R other) ctx)
