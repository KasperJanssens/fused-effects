{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE EmptyDataDecls #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module InterfaceAdapters.Database where

import Control.Monad (void)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Trans.Reader (ReaderT)
import Data.Time.Clock
import Database.Persist
import Database.Persist.Sqlite
import Database.Persist.TH

share
  [mkPersist sqlSettings, mkMigrate "migrateAll"]
  [persistLowerCase|
KeyValueDB
    key String
    value String
    UniqueKey key
    deriving Show
ReservationDB
    day UTCTime
    name String
    email String
    quantity Int
    deriving Show
|]

deleteByKeyAction :: (MonadIO m) => String -> ReaderT SqlBackend m ()
deleteByKeyAction k = deleteBy (UniqueKey k)

insertKeyValueAction :: (MonadIO m) => (String, String) -> ReaderT SqlBackend m ()
insertKeyValueAction (k, v) =
  let scriptDb = KeyValueDB k v
   in void $ insert scriptDb

findByKeyAction :: (MonadIO m) => String -> ReaderT SqlBackend m (Maybe String)
findByKeyAction k = do
  maybeEntity <- getBy (UniqueKey k)
  return $ fmap (keyValueDBValue . entityVal) maybeEntity

listAction :: (MonadIO m) => ReaderT SqlBackend m [Entity KeyValueDB]
listAction = selectList [] []

initDatabase :: SqliteConnectionInfo -> IO ()
initDatabase sqliteConnectionInfo = runSqliteInfo sqliteConnectionInfo $ runMigration migrateAll

insertKeyValue :: SqliteConnectionInfo -> (String, String) -> IO ()
insertKeyValue sqliteConnectionInfo keyValue = runSqliteInfo sqliteConnectionInfo $ insertKeyValueAction keyValue

deleteKeyValue :: SqliteConnectionInfo -> String -> IO (Maybe String)
deleteKeyValue sqliteConnectionInfo k =
  runSqliteInfo sqliteConnectionInfo $ do
    res <- findByKeyAction k
    deleteByKeyAction k
    return res

findKeyValue :: SqliteConnectionInfo -> String -> IO (Maybe String)
findKeyValue sqliteConnectionInfo k = runSqliteInfo sqliteConnectionInfo $ findByKeyAction k

listKeyValues :: SqliteConnectionInfo -> IO [(String, String)]
listKeyValues sqliteConnectionInfo = runSqliteInfo sqliteConnectionInfo $ do
  entities <- listAction
  let entityVals = fmap entityVal entities
  return $ fmap (\e -> (keyValueDBKey e, keyValueDBValue e)) entityVals
