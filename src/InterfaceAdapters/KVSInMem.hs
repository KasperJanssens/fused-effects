{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE GADTs #-}

module InterfaceAdapters.KVSInMem where

import Control.Algebra ((:+:) (..), Algebra, alg)

import Control.Monad.IO.Class (MonadIO)
import qualified Data.Map as Map
import Data.Map (Map)
import UseCases.KVS
import Control.Carrier.State.Strict (StateC, runState, state)

type MyMap = Map String String

runInMem :: KVSInMem m a -> m (MyMap, a)
runInMem kvs = let stateC = runKvsInMem kvs in runState Map.empty stateC

newtype KVSInMem m a = KVSInMem {runKvsInMem :: StateC MyMap m a}
  deriving (Applicative, Functor, Monad, MonadIO)

instance Algebra sig m => Algebra (KVS :+: sig) (KVSInMem m) where
  alg hdl sig ctx = KVSInMem $ case sig of
    L (Read k) -> state $ \m -> (m, Map.lookup k m <$ ctx)
    L (Write k v) -> state $ \m -> (Map.insert k v m, ctx)
    L (Delete k) -> state $ \m -> let maybeElem = Map.lookup k m in (Map.delete k m, maybeElem <$ ctx)
    L List -> state $ \m -> (m, Map.toList m <$ ctx)
    R other -> alg (runKvsInMem . hdl) (R other) ctx
