{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE GADTs #-}

module InterfaceAdapters.KVSSqlite where

import Control.Algebra (Algebra, (:+:)(..), alg)
import Control.Carrier.Reader (ReaderC, ask, runReader)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Database.Persist.Sqlite (SqliteConnectionInfo)
import UseCases.KVS
import qualified InterfaceAdapters.Database as DB

runSqliteC :: SqliteConnectionInfo -> KVSSqliteC m a -> m a
runSqliteC sqliteConnectionInfo kvsSqlite = runReader sqliteConnectionInfo $ runKvsSqlite kvsSqlite

newtype KVSSqliteC m a = KVSSqliteC {runKvsSqlite :: ReaderC SqliteConnectionInfo m a}
  deriving (Applicative, Functor, Monad, MonadIO)

instance (MonadIO m, Algebra sig m) => Algebra (KVS :+: sig) (KVSSqliteC m) where
  alg hdl sig ctx = case sig of
    L (Read k) -> KVSSqliteC ask >>= \sqliteInfo -> (<$ ctx) <$> liftIO (DB.findKeyValue sqliteInfo k)
    L (Delete k) -> KVSSqliteC ask >>= \sqliteInfo -> (<$ ctx) <$> liftIO (DB.deleteKeyValue sqliteInfo k)
    L (Write k v) -> KVSSqliteC ask >>= \sqliteInfo -> (<$ ctx) <$> liftIO (DB.insertKeyValue sqliteInfo (k, v))
    L List -> KVSSqliteC ask >>= \sqliteInfo -> (<$ ctx) <$> liftIO (DB.listKeyValues sqliteInfo)
    R other -> KVSSqliteC $ alg (runKvsSqlite . hdl) (R other) ctx
