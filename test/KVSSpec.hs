{-# LANGUAGE OverloadedStrings #-}

module KVSSpec where

import Control.Concurrent (threadDelay)
import Control.Monad (void, when)
import Data.Text as Text
import Database.Persist.Sqlite (mkSqliteConnectionInfo)
import qualified System.Directory as Dir
import Test.Hspec (Spec, describe, hspec, it, shouldBe)
import InterfaceAdapters.Database (initDatabase)
import UseCases.KVS as KVS
import UseCases.Log as Log
import InterfaceAdapters.KVSInMem
import InterfaceAdapters.KVSSqlite
import qualified InterfaceAdapters.LogConsoleOut as Cout
import qualified InterfaceAdapters.LogInMem as MemLog
import Control.Algebra (Has)

kvDBName :: Text
kvDBName = "kvDB.sqlite"

deleteFileIfExists :: Text -> IO ()
deleteFileIfExists dbName = do
  filePath <- Dir.makeAbsolute $ Text.unpack dbName
  exists <- Dir.doesPathExist filePath
  when exists $ Dir.removeFile filePath

snippet :: (Has KVS sig m, Has Log sig m) => m [(String, String)]
snippet = do
        KVS.write "koekoek" "merel"
        Log.info "Written it"
        KVS.write "adelaar" "worst"
        void $ KVS.delete "adelaar"
        KVS.list

spec :: Spec
spec = describe "" $ do
  it "Run snippet fully in memory" $ do
    (m, (logs, res)) <- runInMem . MemLog.runLog $ snippet
    res `shouldBe` [("koekoek", "merel")]
    logs `shouldBe` ["Info Written it"]
  it "Run snippet console out and with sqlite behind it" $ do
    deleteFileIfExists kvDBName
    let sqliteConnectionInfo = mkSqliteConnectionInfo kvDBName
    initDatabase sqliteConnectionInfo
    res <- runSqliteC sqliteConnectionInfo . Cout.runTraceConsoleOut $ snippet
    res `shouldBe` [("koekoek", "merel")]
