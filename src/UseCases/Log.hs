{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE GADTs #-}

module UseCases.Log where

import Data.Kind (Type)
import Control.Algebra (Has, send)

data Log (m :: Type -> Type) a where
  Error :: String -> Log m ()
  Info :: String -> Log m ()
  Debug :: String -> Log m ()
  Warn :: String -> Log m ()

error :: Has Log sig m => String -> m ()
error m = send (Error m)

info :: Has Log sig m => String -> m ()
info m = send (Info m)

debug :: Has Log sig m => String -> m ()
debug m = send (Debug m)

warn :: Has Log sig m => String -> m ()
warn m = send (Warn m)