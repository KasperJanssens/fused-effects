{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE KindSignatures #-}

module UseCases.KVS where

import Control.Algebra
import Data.Kind (Type)
import Data.Map (Map)
import qualified Data.Map as Map

data KVS (m :: Type -> Type) a where
  Read :: String -> KVS m (Maybe String)
  Write :: String -> String -> KVS m ()
  Delete :: String -> KVS m (Maybe String)
  List :: KVS m [(String, String)]

read :: Has KVS sig m => String -> m (Maybe String)
read k = send (Read k)

write :: Has KVS sig m => String -> String -> m ()
write k v = send (Write k v)

delete :: Has KVS sig m => String -> m (Maybe String)
delete k = send (Delete k)

list :: Has KVS sig m => m [(String, String)]
list = send List
